package com.rebootmc.lstattrak.command.subcommand;

import com.rebootmc.lstattrak.LStatTrakPlugin;
import com.rebootmc.lstattrak.command.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreditsCommand extends SubCommand
{

	public CreditsCommand(LStatTrakPlugin plugin)
	{
		super(plugin, "LStatTrak.coins", "coins", "credits");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (args.length < 2 && !(sender instanceof Player))
		{
			sender.sendMessage(plugin.getMessage("messages.usageCredits"));
		} else
		{
			Player target = args.length < 2 ? (Player) sender : Bukkit.getPlayer(args[1]);

			if (target != sender && !sender.hasPermission("LStatTrak.credits.others"))
			{
				sender.sendMessage(plugin.getMessage("messages.permission"));
			} else if (target == null)
			{
				sender.sendMessage(plugin.getMessage("messages.targetNotFound"));
			} else
			{
				int coins   = plugin.getCoinsFile().getCoins(target);
				int renames = plugin.getRenameFile().getRedeems(target);
				sender.sendMessage(plugin.getMessage("messages.credits")
						.replace("[player]", target.getName())
						.replace("[redeems]", Integer.toString(coins))
						.replace("[renames]", Integer.toString(renames)));
			}
		}
		return true;
	}
}
