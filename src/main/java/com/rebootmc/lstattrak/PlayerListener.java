package com.rebootmc.lstattrak;

import com.rebootmc.lstattrak.util.RegexMatch;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Handles various events surrounding player StatTrak items
 */
public class PlayerListener implements Listener
{

	//    Map<Player, List<ItemStack>> giveBack = new HashMap<>();
	private LStatTrakPlugin plugin;

	public PlayerListener(LStatTrakPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
		Player dead = event.getEntity();

		if (dead != null)
		{
			ItemStack hand = dead.getItemInHand();

			if (hand != null && plugin.isStatTracked(hand))
			{
				RegexMatch match = plugin.matches.get(hand.getType());
				dead.setItemInHand(plugin.incrementValue(hand, plugin.DEATHS_REGEX, "deaths", match.getDeathsText(), 1));
			}
		}

		Player            killer    = null;
		EntityDamageEvent lastCause = dead.getLastDamageCause();

		if (lastCause instanceof EntityDamageByEntityEvent)
		{
			EntityDamageByEntityEvent edbe    = (EntityDamageByEntityEvent) lastCause;
			Entity                    damager = edbe.getDamager();

			if (damager instanceof Player)
			{
				killer = (Player) damager;
			} else if (damager instanceof Projectile)
			{
				Projectile       projectile = (Projectile) damager;
				ProjectileSource shooter    = projectile.getShooter();

				if (shooter instanceof Player)
				{
					killer = (Player) shooter;
				}
			}
		}

		if (killer != null)
		{
			ItemStack hand = killer.getItemInHand();

			if (hand != null && plugin.isStatTracked(hand))
			{
				RegexMatch match = plugin.matches.get(hand.getType());
				killer.setItemInHand(plugin.incrementValue(plugin.incrementValue(hand, plugin.DURA_REGEX, "durability", match.getDurabilityText(), getDurabilityPercent(hand)),
						plugin.KILLS_REGEX, "kills", match.getKillsLoreText(), 1));
			}
		}
	}

	public double getDurabilityPercent(ItemStack stack)
	{
		double max = stack.getType().getMaxDurability();
		return ((max - ((double) stack.getDurability())) / max) * 100;
	}

	@EventHandler
	public void onPlayerDeathSaveItems(PlayerDeathEvent event)
	{
		final Player          dead  = event.getEntity();
		Iterator<ItemStack>   drops = event.getDrops().iterator();
		final List<ItemStack> ret   = new ArrayList<ItemStack>();

		while (drops.hasNext())
		{
			ItemStack itm = drops.next();
			if (plugin.isStatTracked(itm))
			{
				drops.remove();
				ret.add(itm);
			}
		}

		Bukkit.getScheduler().runTaskLater(plugin, new Runnable()
		{
			public void run()
			{
				for (ItemStack itm : ret)
				{
					dead.getInventory().addItem(itm);
				}
			}
		}, 1L);
	}

	@EventHandler
	public void onPlayerPickup(PlayerPickupItemEvent event)
	{
		ItemStack item = event.getItem().getItemStack();

		if (!plugin.isStatTracked(item))
		{
			return;
		}

		boolean owner = plugin.isOwner(item, event.getPlayer());

		if (!owner)
		{
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory().getType() == InventoryType.ANVIL)
		{
			Inventory     clicked = event.getClickedInventory();
			InventoryView view    = event.getView();

			if (event.getClick().isShiftClick() && view.getBottomInventory() == clicked)
			{
				ItemStack itm = event.getCurrentItem();

				if (itm != null && plugin.isStatTracked(itm))
				{
					event.setCancelled(true);
				}
			} else if (clicked == view.getTopInventory())
			{
				ItemStack itm = event.getCursor();

				if (itm != null && plugin.isStatTracked(itm))
				{
					event.setCancelled(true);
				}
			}
		}
	}

    /*@EventHandler
    public void onPlayerDeathSave(PlayerDeathEvent e)
    {
        Player          dead  = e.getEntity();
        List<ItemStack> drops = new ArrayList<>();
        drops.addAll(e.getDrops());
        List<ItemStack> ret = new ArrayList<>();
        for (ItemStack i : drops)
        {
            if (plugin.isStatTracked(i))
            {
                List<String> l    = i.getItemMeta().getLore();
                List<String> lore = new ArrayList<>();
                lore.addAll(l);
                e.getDrops().remove(i);
                ret.add(i);
            }
            giveBack.put(dead, ret);
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e)
    {
        Player res = e.getPlayer();
        if (giveBack.containsKey(res))
        {
            for (ItemStack i : giveBack.get(res))
            {
                res.getInventory().addItem(i);
            }
            giveBack.remove(res);
        }
    }*/

	@EventHandler
	public void onBreak(BlockBreakEvent event)
	{
		Player    player = event.getPlayer();
		ItemStack hand   = player.getItemInHand();

		if (hand != null && hand.getType() != Material.AIR)
		{
			if (plugin.isStatTracked(hand))
			{
				RegexMatch match = plugin.matches.get(hand.getType());
				player.setItemInHand(plugin.incrementValue(plugin.incrementValue(hand, plugin.DURA_REGEX, "durability", match.getDurabilityText(), getDurabilityPercent(hand)),
						plugin.BLOCKS_REGEX, "blocks", match.getBlocksBrokenText(), 1));
			}
		}
	}

	@EventHandler
	public void onMobDeath(EntityDeathEvent event)
	{
		if (event.getEntityType() == EntityType.PLAYER)
		{
			return;
		}

		Player killer = event.getEntity().getKiller();
		if (killer != null)
		{
			ItemStack hand = killer.getItemInHand();

			if (hand != null && hand.getType() != Material.AIR)
			{
				if (plugin.isStatTracked(hand))
				{
					RegexMatch match = plugin.matches.get(hand.getType());
					killer.setItemInHand(plugin.incrementValue(plugin.incrementValue(hand, plugin.DURA_REGEX, "durability", match.getDurabilityText(), getDurabilityPercent(hand)),
							plugin.MOBS_REGEX, "mob", match.getMobKillsText(), 1));
				}
			}
		}
	}
}
