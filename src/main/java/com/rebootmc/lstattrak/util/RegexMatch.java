package com.rebootmc.lstattrak.util;

public class RegexMatch
{
	private String ownerText, killsLoreText, blocksBrokenText, durabilityText, mobKillsText, deathsText;

	public String getBlocksBrokenText()
	{
		return blocksBrokenText;
	}

	public void setBlocksBrokenText(String blocksBrokenText)
	{
		this.blocksBrokenText = blocksBrokenText;
	}

	public String getDurabilityText()
	{
		return durabilityText;
	}

	public void setDurabilityText(String durabilityText)
	{
		this.durabilityText = durabilityText;
	}

	public String getKillsLoreText()
	{
		return killsLoreText;
	}

	public void setKillsLoreText(String killsLoreText)
	{
		this.killsLoreText = killsLoreText;
	}

	public String getOwnerText()
	{
		return ownerText;
	}

	public void setOwnerText(String ownerText)
	{
		this.ownerText = ownerText;
	}

	public String getMobKillsText()
	{
		return mobKillsText;
	}

	public void setMobKillsText(String mobKillsText)
	{
		this.mobKillsText = mobKillsText;
	}

	public String getDeathsText()
	{
		return deathsText;
	}

	public void setDeathsText(String deathsText)
	{
		this.deathsText = deathsText;
	}
}
