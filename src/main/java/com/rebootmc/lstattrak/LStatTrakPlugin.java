package com.rebootmc.lstattrak;

import com.rebootmc.lstattrak.command.CommandHandler;
import com.rebootmc.lstattrak.storage.CoinsFile;
import com.rebootmc.lstattrak.storage.RenameFile;
import com.rebootmc.lstattrak.util.RegexMatch;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LStatTrakPlugin extends JavaPlugin
{

	public HashMap<Material, RegexMatch> matches = new HashMap<>();
	public Pattern DURA_REGEX, MOBS_REGEX, KILLS_REGEX, OWNER_REGEX, BLOCKS_REGEX, DISPLAY_NAME_REGEX, DEATHS_REGEX;
	String displayName;
	private CoinsFile  file;
	private RenameFile redeems;
	private List<String> baseLore = new ArrayList<String>();

	private File              savedFile;
	private YamlConfiguration savedYaml;

	@Override
	public void onEnable()
	{
		saveDefaultConfig();
		build();
		this.file = new CoinsFile(this, "coins.yml");
		this.redeems = new RenameFile(this, "redeems.yml");
		this.savedFile = new File(getDataFolder(), "saved.yml");

		if (!savedFile.exists())
		{
			try
			{
				savedFile.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		this.savedYaml = YamlConfiguration.loadConfiguration(savedFile);

		// register commands and listeners
		getCommand("LStatTrak").setExecutor(new CommandHandler(this));
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		getServer().getPluginManager().registerEvents(new CombatTagPlusListener(this), this);
	}

	private void build()
	{
		getLogger().info("Building all name and lore patterns");
		this.displayName = getConfig().getString("settings.LStatTrak.name");
		RegexMatch match = new RegexMatch();
		displayName = (ChatColor.translateAlternateColorCodes('&', displayName));
		DISPLAY_NAME_REGEX = (Pattern.compile(displayName.replace("[custom]", "([a-zA-Z0-9 ]+)")));
		for (String s : getConfig().getConfigurationSection("items").getKeys(false))
		{
			for (String str : getConfig().getStringList("items." + s))
			{
				if (str.contains("[kills]"))
				{
					match.setKillsLoreText(ChatColor.translateAlternateColorCodes('&', str));
					KILLS_REGEX = (Pattern.compile(match.getKillsLoreText().replace("[kills]", "([0-9]+)")));
				}
				if (str.contains("[mob]"))
				{
					match.setMobKillsText(ChatColor.translateAlternateColorCodes('&', str));
					MOBS_REGEX = (Pattern.compile(match.getMobKillsText().replace("[mob]", "([0-9]+)")));
				}

				if (str.contains("[blocks]"))
				{
					match.setBlocksBrokenText(ChatColor.translateAlternateColorCodes('&', str));
					BLOCKS_REGEX = (Pattern.compile(match.getBlocksBrokenText().replace("[blocks]", "([0-9]+)")));
				}

				if (str.contains("[durability]"))
				{
					match.setDurabilityText(ChatColor.translateAlternateColorCodes('&', str));
					DURA_REGEX = (Pattern.compile(match.getDurabilityText().replace("[durability]", "([0-9]+)")));
				}

				if (str.contains("[player]"))
				{
					match.setOwnerText(ChatColor.translateAlternateColorCodes('&', str));
					//OWNER_REGEX = (Pattern.compile(match.getOwnerText().replace("[player]", "(.*?)\\w")));
					OWNER_REGEX = (Pattern.compile(match.getOwnerText().replace("[player]", "([a-zA-Z0-9 ]+)")));
				}

				if (str.contains("[deaths]"))
				{
					match.setDeathsText(ChatColor.translateAlternateColorCodes('&', str));
					DEATHS_REGEX = Pattern.compile(match.getDeathsText().replace("[deaths]", "([0-9]+)"));
				}
				matches.put(Material.valueOf(s), match);
			}
		}
	}

	@Override
	public void onDisable()
	{
		try
		{
			savedYaml.save(savedFile);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void saveItem(Player player, ItemStack item)
	{
		List<ItemStack> items = getItems(player);
		items.add(item);
		savedYaml.set(player.getUniqueId().toString(), items);
	}

	public List<ItemStack> getItems(Player player)
	{
		String id = player.getUniqueId().toString();
		if (!savedYaml.isList(id))
		{
			return new ArrayList<ItemStack>();
		}

		return (List<ItemStack>) savedYaml.getList(id);
	}

	public void clearSaved(Player player)
	{
		savedYaml.set(player.getUniqueId().toString(), null);
	}

	public CoinsFile getCoinsFile()
	{
		return file;
	}

	public RenameFile getRenameFile()
	{
		return redeems;
	}

	public boolean canBeTracked(ItemStack item)
	{
		if (item == null)
		{
			return false;
		}

		if (isStatTracked(item))
			return false;

		return getConfig().contains("items." + item.getType().name());
	}

	public boolean isStatTracked(ItemStack item)
	{
		ItemMeta meta = item.getItemMeta();
		if (meta != null && meta.hasDisplayName())
		{
			Matcher matcher = DISPLAY_NAME_REGEX.matcher(meta.getDisplayName());

			return matcher.matches();
		}
		return false;
	}

	public ItemStack setupItem(ItemStack item, Player owner)
	{
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName.replace("[custom]", "Custom Text"));

		List<String> lore        = (meta.getLore() != null & meta.hasLore()) ? meta.getLore() : new ArrayList<String>();
		List<String> configEntry = getConfig().getStringList("items." + item.getType().name());
		for (String str : configEntry)
		{
			lore.add(ChatColor.translateAlternateColorCodes('&', str.replace("[kills]", "0")
					.replace("[player]", owner.getName())
					.replace("[mob]", "0")
					.replace("[blocks]", "0")
					.replace("[durability]", "100")
					.replace("[deaths]", "0")));
		}
		meta.spigot().setUnbreakable(true);
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}

	public ItemStack changeName(ItemStack item, String name)
	{
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName.replace("[custom]", name));
		item.setItemMeta(meta);
		return item;
	}

	public ItemStack claimItem(ItemStack stack, Player claimer)
	{
		ItemMeta     itemMeta = stack.getItemMeta();
		List<String> parsed   = new ArrayList<String>();

		for (String str : itemMeta.getLore())
		{
			RegexMatch match = matches.get(stack.getType());
			if (OWNER_REGEX.matcher(str).find())
			{
				parsed.add(match.getOwnerText().replace("[player]", claimer.getName()));
			} else
			{
				parsed.add(str);
			}
		}

		itemMeta.setLore(parsed);
		stack.setItemMeta(itemMeta);

		return stack;
	}

	public ItemStack incrementValue(ItemStack item, Pattern pattern, String name, String text, double value)
	{
		if (pattern == null)
		{
			return item;
		}

		ItemMeta     meta   = item.getItemMeta();
		List<String> lore   = meta.getLore();
		List<String> parsed = new ArrayList<String>();

		for (String str : lore)
		{
			Matcher matcher = pattern.matcher(str);
			if (matcher.find())
			{
				int i = Integer.parseInt(matcher.group(1));
				parsed.add(text.replace("[" + name + "]", String.valueOf(name.equals("durability") ? (int) value : (int) (i + value))));
			} else
			{
				parsed.add(str);
			}
		}

		meta.setLore(parsed);
		item.setItemMeta(meta);
		return item;
	}

	public boolean isOwner(ItemStack item, Player player)
	{
		ItemMeta     meta   = item.getItemMeta();
		List<String> lore   = meta.getLore();
		List<String> parsed = new ArrayList<String>();

		Pattern pattern = OWNER_REGEX;

		for (String str : lore)
		{
			Matcher matcher = pattern.matcher(str);

			if (matcher.find())
			{
				String owner = matcher.group(1);

				if (player.getName().equalsIgnoreCase(owner))
				{
					return true;
				}
			}
		}

		return false;
	}

	public String getMessage(String path)
	{
		return ChatColor.translateAlternateColorCodes('&', getConfig().getString(path));
	}
}
